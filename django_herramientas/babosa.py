from unicodedata import normalize
import re


def norm_unicode(palabra):
    return normalize('NFKD', palabra)


def norm_utf8(palabra):
    return normalize('NFKC', palabra)


def babosear(palabra):
        # unicode de la 'a' a la 'z'
    conjunto_valido = {i for i in range(97, 123)} | {i for i in range(48, 57)}
    conjunto_valido.add(ord('_'))
    palabra = palabra.lower().replace(' ', '_')
    regex = re.compile(r'_+')
    # reemplazar, devuelve otro string corregido
    palabra_corregida = regex.sub('_', palabra)
    palabra_unicode = norm_unicode(palabra_corregida)
    palabra_unicode = norm_unicode(palabra_valida)
    valores_descartes = [771, 776, 769]
    lista_unicode = list(map(ord, palabra_unicode))
    # print(lista_unicode)
    lista_limpia = [x for x in lista_unicode
                    if x not in valores_descartes and x in conjunto_valido]
    # print(lista_limpia)
    palabra_limpia = ''.join(map(chr, lista_limpia))
    palabra_utf8 = norm_utf8(palabra_limpia)
    return palabra_utf8
