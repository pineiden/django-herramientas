from django.conf import settings

def global_values(request):
    my_dict = {
        'site_url': settings.SITE_URL,
        'site_name': settings.SITE_NAME,
        'google_api': settings.GOOGLE_API_KEY,
        'contacto': settings.CONTACTO,
        'sitio': settings.SITIO,
        'nav':settings.NAV_MENU,
        'nv': settings.NV,
        'media_url': settings.MEDIA_URL
    }

    return my_dict
