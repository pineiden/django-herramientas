from setuptools import setup, find_packages
setup(
    name="django_herramientas",
    version="0.1",
    packages=find_packages(),
    author="David Pineda",
    author_email="dpineda@uchile.cl",
)
